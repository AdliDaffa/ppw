from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Mohammad Adli Daffa' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 5, 2) #TODO Implement this, format (Year, Month, Date)
npm = 1706984663 # TODO Implement this
univ = "Universitas Indonesia"
hobby = "Koding"
desc = "Assalamualaikum. Nama gua Mohammad Adli, biasa dipanggil adli. Milih fasilkom ui karena prospek pekerjaannya pas lulus nanti lumayan bagus, padahal sebelumnya pengen di fmipa ui. Gua suka misteri atau teka-teki, pas masuk fasilkom tersalurkan lah kesukaan gua. Banyak misteri di fasilkom termasuk juga banyak teka teki di mata kuliah yang ada di dalamnya. Gua harap gua bisa lulus dan dapat bekerja di perusahaan it yang sesuai dengan bakat gua yang insyaAllah didapatkan di fasilkom."

name1 = "Adam Maulana"
age1 = 18
npm1 = 1706043651
hobby1 = "Membaca"
desc1 = "Kenalin nama gue Adam, gua masuk sistem informasi fasilkom karena gua diterima di sistem informasi fasilkom. Cita cita gua jadi iron man. Keseharian gua itu ya mencoba lebih baik dari hari sebelumnya, walau kadang ga lebih baik, seringnya lebih ga baik, karena kebaikan datangnya dari Allah."

name2 = "Muhammad Nadhirsyah Indra"
age2 = 18
npm2 = 1706039383
hobby2 = "Main game"
desc2 = "gue baru kepikiran masuk ilmu komputer itu kelas 12. gue milih ilmu komputer karena prospek kerjanya luas banget. gue gak masalah kalo gue diterima dimana aja soalnya gue lebih mentingin cara gue belajar daripada universitasnya."

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'univ': univ,
    'hobby': hobby, 'desc': desc, 'name1': name1, 'name2': name2, 'age1': age1, 'age2': age2, 'npm1':
    npm1, 'npm2': npm2, 'hobby1': hobby1, 'hobby2': hobby2, 'desc1': desc1, 'desc2': desc2}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
